package cmd

import (
	"log"
	"rabbitmq_test/client"
	"regexp"
	"strconv"

	"github.com/spf13/cobra"
	"github.com/streadway/amqp"
)

var consumeCmd = &cobra.Command{
	Use:   "consume",
	Long:  "Command for RabbitMQ consumer with consuming interval",
	Short: "Mocking RabbitMQ consumer",
	Run:   runConsume,
}

const (
	ACK_MODE_DO_NOTHING   = 0 // Highly NOT recommended. It will block the queue when one of the consumers keeps fetching messages without ACK or NACK
	ACK_MODE_ACK          = 1
	ACK_MODE_NACK         = 2
	ACK_MODE_NACK_REQUEUE = 3
)

var consumingMessageNum int64
var ackMode int = ACK_MODE_ACK
var consumingSteps []string
var consumeFromQueue string

func init() {
	consumeCmd.Flags().Int64VarP(&consumingMessageNum, "message-num", "n", 0, "total number of messages to consume, 0 meaning unlimited")
	consumeCmd.Flags().IntVarP(&ackMode, "ack-mode", "a", ACK_MODE_ACK, "mode of acknowledgement, can be 0 (do nothing), 1 (acked, default), 2 (not acked & drop msg) or 3 (not acked & requeue msg)")
	consumeCmd.Flags().StringArrayVarP(&consumingSteps, "consuming-steps", "s", []string{}, "steps of consuming messages, each step should be formatted as `CONSUMING_NO[,ACK_MODE]`, e.g. -s 10 -s 5,2 -s 20,1 means consuming 10 messages and ACK them, then consuming 5 messages and NACK them, and then consuming 20 messages and ACK them")
	consumeCmd.Flags().StringVarP(&consumeFromQueue, "queue", "q", "", "custom queue name")
}

var runConsume = func(cmd *cobra.Command, args []string) {

	if consumingMessageNum > 0 && len(consumingSteps) > 0 {
		log.Fatalln("--consuming-steps | -s should not be used together with --message-num | -n")
	}

	queueName := defaultQueueName
	if len(produceToQueue) > 0 {
		queueName = produceToQueue
	}
	client, err := client.OpenNewRabbitMQClient(queueName, true)
	if err != nil {
		panic(err)
	}
	defer client.CloseWithConnection()

	messages, err := client.ConsumeMessages()
	if err != nil {
		log.Println(err)
	}

	if len(consumingSteps) == 0 {
		ct := consumeWithoutSteps(messages, consumingMessageNum, ackMode)
		log.Println("total consumed", ct, "messages with ack mode:", ackMode)
	} else {
		ct := consumeBySteps(messages, consumingSteps)
		log.Println("total consumed", ct, "messages")
	}
}

func applyAckModeToMessage(message amqp.Delivery, ackMode int) {
	switch ackMode {
	case ACK_MODE_ACK:
		message.Ack(false)
	case ACK_MODE_NACK:
		message.Nack(false, false)
	case ACK_MODE_NACK_REQUEUE:
		message.Nack(false, true)
	default:
	}
}

func consumeWithoutSteps(messages <-chan amqp.Delivery, num int64, mode int) (consumedCount int) {
	hasLimitedConsumingNum := num > 0
	cNum := num

	for message := range messages {
		log.Printf("received message: %s", message.Body)
		consumedCount++
		applyAckModeToMessage(message, mode)
		if hasLimitedConsumingNum {
			cNum--
			if cNum == 0 {
				break
			}
		}
	}
	return
}

func consumeBySteps(messages <-chan amqp.Delivery, steps []string) (consumedCount int) {
	stepRegex := regexp.MustCompile(`(?P<num>\d+),?(?P<mode>\d+)?`)
	getStepNumAndAckMode := func(str string) (num int64, mode int) {
		mode = ACK_MODE_ACK
		match := stepRegex.FindStringSubmatch(str)
		for i, name := range stepRegex.SubexpNames() {
			if name == "num" {
				num, _ = strconv.ParseInt(match[i], 10, 64)
			} else if name == "mode" {
				if len(match[i]) > 0 {
					mode, _ = strconv.Atoi(match[i])
				}
			}
		}
		return
	}
	for step, stepStr := range steps {
		num, mode := getStepNumAndAckMode(stepStr)
		log.Printf("Step %d: consuming %d messages with ack mode %d", step, num, mode)
		consumeWithoutSteps(messages, num, mode)
	}
	return
}
