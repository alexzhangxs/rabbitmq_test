package cmd

import "github.com/spf13/cobra"

var RootCmd = &cobra.Command{}

var defaultQueueName = "DemoQueue"

func init() {
	RootCmd.AddCommand(producerCmd)
	RootCmd.AddCommand(consumeCmd)
}
