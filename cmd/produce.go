package cmd

import (
	"fmt"
	"log"
	"rabbitmq_test/client"
	"time"

	"github.com/spf13/cobra"
)

var producerCmd = &cobra.Command{
	Use:   "produce",
	Long:  "Command for RabbitMQ producer with provided message number and interval",
	Short: "Mocking RabbitMQ producer",
	Run:   runProduce,
}

var producingMessageNum int64
var produceToQueue string

func init() {
	producerCmd.Flags().Int64VarP(&producingMessageNum, "message-num", "n", 0, "total number of messages to produce")
	producerCmd.Flags().StringVarP(&produceToQueue, "queue", "q", "", "custom queue name")
}

var runProduce = func(cmd *cobra.Command, args []string) {

	queueName := defaultQueueName
	if len(produceToQueue) > 0 {
		queueName = produceToQueue
	}
	client, err := client.OpenNewRabbitMQClient(queueName, true)
	if err != nil {
		panic(err)
	}
	defer client.CloseWithConnection()

	timeNowStr := time.Now().Format("20060102150405")

	for i := int64(0); i < producingMessageNum; i++ {
		message := fmt.Sprintf("%s mocking message index %d", timeNowStr, i)
		if err := client.SendStringMessage(message); err != nil {
			log.Fatalln("send message error", err, "index", i)
		}
		log.Printf("message index %d sent", i)
	}
}
