## Description

This project is aimed for testing rabbitMQ service in docker containers with limited memory resources and requirement of ensuring data persistence.

To use as little memory as possible, the RabbitMQ server needs to apply a policy to turn on "Lazy Mode" for each queue, and set the memory limitation as `vm_memory_high_watermark.absolute` in config file.

## Structure

There are two containers: `demo-rabbitmq` as RabbitMQ server and `demo-client` as RabbitMQ client with commands to act as either producer or consumer.

```mermaid
graph LR
A(Client Producer) --> |Push Messages| B(RabbitMQ Server)
C(Client Consumer) --> |Pull Messages| B
```

## Prerequisite

Docker Desktop for Mac recommended

## Setup & Execution

1. To initially start or rebuild & restart containers, run
    `$ make start`
    
    * When containers are created successfully, you may check http://localhost:15672/#/queues (username: admin, password: password) to view the information of queues in RabbitMQ management UI.

    * To monitor the memory usage of RabbitMQ server, you may either view the "Stats" tab of container `demo-rabbitmq` in Dashboard of Docker Desktop, or run
    `$ make monitor-mem`
    in a new terminal.

2. To perform RabbitMQ client functions (e.g. produce essages or consume messages), run
    `$ make exec`
    
    * To produce messages, run
    `$ mq_client produce [FLAGS]`

    * To consume messages, run
    `$ mq_client consume [FLAGS]`

    For example, you may run
    `$ mq_client produce -n 10000 -q Queue1`
    to produce 10000 mocking messages to queue "Queue1", and then run
    `$ mq_client consume -n 1000 -q Queue1`
    to consume 1000 messages from queue "Queue1"

    For more details about the flags, you may run `$ mq_client produce -h` or `$ mq_client consume -h`
    