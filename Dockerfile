FROM golang:1.16.7-alpine3.14
RUN apk add --no-cache bash

RUN mkdir -p /go/bin
ENV GOPATH=/go
ENV GOBIN=$GOPATH/bin

ADD . /app/src
WORKDIR /app/src
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go install .
RUN mv ${GOBIN}/rabbitmq_test ${GOBIN}/mq_client
RUN rm -rf /app/src

ENTRYPOINT [ "tail", "-f", "/dev/null" ]
