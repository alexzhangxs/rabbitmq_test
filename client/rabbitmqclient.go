package client

import (
	"errors"
	"log"
	"os"
	"time"

	"github.com/streadway/amqp"
)

type RabbitMQClient struct {
	*amqp.Channel
	connection      *amqp.Connection
	queue           amqp.Queue
	noteConfirmChan chan amqp.Confirmation
	noteReturnChan  chan amqp.Return
}

func OpenNewRabbitMQClient(queueName string, isProducer bool) (*RabbitMQClient, error) {
	// Define RabbitMQ server URL.
	amqpServerURL := os.Getenv("AMQP_SERVER_URL")

	// Create a new RabbitMQ connection.
	connectRabbitMQ, err := amqp.Dial(amqpServerURL)
	if err != nil {
		return nil, err
	}

	// Opening a channel to our RabbitMQ instance over
	// the connection we have already established.
	channelRabbitMQ, err := connectRabbitMQ.Channel()
	if err != nil {
		return nil, err
	}

	queue, err := channelRabbitMQ.QueueDeclare(
		queueName, true, false, false, false, amqp.Table{},
	)
	if err != nil {
		return nil, err
	}

	channel := &RabbitMQClient{
		Channel:    channelRabbitMQ,
		connection: connectRabbitMQ,
		queue:      queue,
	}

	if isProducer {
		channelRabbitMQ.Confirm(false)
		channel.noteConfirmChan = channelRabbitMQ.NotifyPublish(make(chan amqp.Confirmation, 1))
		channel.noteReturnChan = channelRabbitMQ.NotifyReturn(make(chan amqp.Return))
	} else {
		channel.Qos(10, 0, true)
	}

	return channel, nil
}

func (c *RabbitMQClient) QueueName() string {
	return c.queue.Name
}

func (c *RabbitMQClient) SendStringMessage(msg string) error {
	message := amqp.Publishing{
		ContentType:  "text/plain",
		Body:         []byte(msg),
		DeliveryMode: amqp.Persistent, // NOTE: PLEASE MAKE SURE SELECT PERSISTENT DELIVERY MODE FOR EVERY MESSAGE
	}

	if err := c.Publish(
		"",            // exchange
		c.QueueName(), // queue name
		false,         // mandatory
		false,         // immediate
		message,       // message to publish
	); err != nil {
		return errors.New("failed to publish message to exchange/queue")
	}

	select {
	case ntf := <-c.noteConfirmChan:
		if !ntf.Ack {
			return errors.New("failed to deliver message to exchange/queue")
		}
	case <-c.noteReturnChan:
		return errors.New("failed to deliver message to exchange/queue")
	case <-time.After(time.Second):
		log.Println("message delivery confirmation to exchange/queue timed out")
	}
	return nil
}

func (c *RabbitMQClient) ConsumeMessages() (<-chan amqp.Delivery, error) {
	return c.Consume(
		c.QueueName(), // queue name
		"",            // consumer
		false,         // auto-ack // NOTE: WE NEED TO ACKNOWLEDGE THE MESSAGE MANUALLY
		false,         // exclusive
		false,         // no local
		false,         // no wait
		nil,           // arguments
	)
}

func (c *RabbitMQClient) CloseWithConnection() (err error) {
	defer func() {
		if c.connection != nil {
			if cErr := c.connection.Close(); cErr != nil && err == nil {
				err = cErr
			}
		}
	}()
	err = c.Close()
	return
}
