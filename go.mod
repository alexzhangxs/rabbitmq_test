module rabbitmq_test

go 1.16

require (
	github.com/spf13/cobra v1.4.0
	github.com/streadway/amqp v1.0.0
)
