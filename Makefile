#!make

.PHONY: start
start:
	docker-compose up -d --build

.PHONY: exec
exec:
	docker exec -it demo-client bash 

.PHONY: monitor-mem
monitor-mem:
	docker stats --format "table {{.Container}}\t{{.MemUsage}}" demo-rabbitmq
